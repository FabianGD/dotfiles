# Sample .bashrc for SuSE Linux
# Copyright (c) SuSE GmbH Nuernberg

# There are 3 different types of shells in bash: the login shell, normal shell
# and interactive shell. Login shells read ~/.profile and interactive shells
# read ~/.bashrc; in our setup, /etc/profile sources ~/.bashrc - thus all
# settings made here will also take effect in a login shell.
#
# NOTE: It is recommended to make language settings in ~/.profile rather than
# here, since multilingual X sessions would not work properly if LANG is over-
# ridden in every subshell.

# Some applications read the EDITOR variable to determine your favourite text
# editor. So uncomment the line below and enter the editor of your choice :-)
#export EDITOR=/usr/bin/vim
#export EDITOR=/usr/bin/mcedit

# For some news readers it makes sense to specify the NEWSSERVER variable here
#export NEWSSERVER=your.news.server

# If you want to use a Palm device with Linux, uncomment the two lines below.
# For some (older) Palm Pilots, you might need to set a lower baud rate
# e.g. 57600 or 38400; lowest is 9600 (very slow!)
#
#export PILOTPORT=/dev/pilot
#export PILOTRATE=115200

function addToPATH {
    case ":$PATH:" in
        *":$1:"*) :;; # already there
        *) PATH="$1:$PATH";; # or PATH="$PATH:$1"
    esac
}

if [ ! -v "${LD_LIBRARY_PATH}" ]; then
  export LD_LIBRARY_PATH
fi

if [ ! -v "${C_INCLUDE_PATH}" ]; then
  export C_INCLUDE_PATH
fi

function addToLIBPATH {
    case ":$LD_LIBRARY_PATH:" in
        *":$1:"*) :;; # already there
        *) LD_LIBRARY_PATH="$1:$LD_LIBRARY_PATH";; # or PATH="$PATH:$1"
    esac
}

function addToCPATH {
    case ":$C_INCLUDE_PATH:" in
        *":$1:"*) :;; # already there
        *) C_INCLUDE_PATH="$1:$C_INCLUDE_PATH";; # or PATH="$PATH:$1"
    esac
}

test -s ~/.alias && . ~/.alias || true

# Dircolors
if [ -d "$HOME/.dircolors" ] ; then
    eval `dircolors ~/.dircolors/dircolors.ansi-dark`
fi

addToPATH $HOME/.local/bin
addToLIBPATH $HOME/.local/lib
addToLIBPATH $HOME/.local/lib64
addToCPATH $HOME/.local/include

# Psi4 setup
export PATH=$HOME/.local/bin:$PATH  # psi4 executable
export PYTHONPATH=$HOME/.local/lib/:$PYTHONPATH  # psi4 pymodule

# Misc commands
alias mkp=$'mkdir -p'
alias ll="ls -lFh"
alias lla="ls -lFah"

unset XMODIFIERS
unset INPUT_METHOD

export TEXMFHOME=$HOME/.texmf

. $HOME/.nix-profile/etc/profile.d/nix.sh
eval "$(starship init bash)"
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
