call plug#begin()
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'preservim/nerdtree'
Plug 'ncm2/ncm2'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'ayu-theme/ayu-vim'
Plug 'tpope/vim-fugitive'
call plug#end()

set termguicolors


" Python2
:let g:loaded_python_provider=0
" :let g:python_host_prog=expand('~/work/venvs/neovim/bin/python3')

" Python3
:let g:python3_host_prog=expand('~/work/venvs/neovim/bin/python3')

let ayucolor="mirage"
colorscheme ayu

hi Normal guibg=NONE ctermbg=NONE

set nu

map <C-b> :NERDTreeToggle<CR>

set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab

autocmd BufWritePre * %s/\s\+$//e

" add yaml stuffs
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
